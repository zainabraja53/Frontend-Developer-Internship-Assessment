const verifyPassword = () => {
    const newP = document.getElementById("newpass").value;
    const confP = document.getElementById("reenterpass").value;
    const alphaNum = /^(?=.*[0-9])[a-zA-Z0-9]{0,15}$/;

    if (newP.length < 8) {
        document.getElementById("error").innerHTML = "Password length must be greater than 8 characters";
        return false;
    }

    if (!newP.match(alphaNum)) {
        document.getElementById("error").innerHTML = "Password must contain atleast 1 number"
        return false;
    }

    if (newP != confP) {
        document.getElementById("reerror").innerHTML = "Passwords do not match"
        return false;
    }
}